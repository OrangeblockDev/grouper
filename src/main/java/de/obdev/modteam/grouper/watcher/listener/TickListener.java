package de.obdev.modteam.grouper.watcher.listener;

/**
 * Created by obdev on 11.07.2016.
 */
public interface TickListener {

    public void tick();

}
