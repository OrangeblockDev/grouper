package de.obdev.modteam.grouper.watcher;

import de.obdev.modteam.grouper.test.master.Master;
import de.obdev.modteam.grouper.watcher.listener.TickListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by obdev on 11.07.2016.
 */
public class Watcher implements Runnable{

    Logger logger = Logger.getLogger(super.getClass().getName());

    public static Long time = 5000L;

    private static String dynamicHash;

    static {
        try {
            dynamicHash = Master.getHash("dynamic");
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private static List<TickListener> listeners = new ArrayList<>();

    @Override
    public void run() {

        while (true) {
            try {
                Thread.sleep(time);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            try {
                if (!Master.getHash("dynamic").equals(dynamicHash)) {
                    dynamicHash = Master.getHash("dynamic");
                    try {
                        Master.call("dynamic");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            for (TickListener listener:listeners) {
                listener.tick();
            }
        }

    }


    public static void addListener(TickListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    public static void removeListener(TickListener listener) {
        if (listeners.contains(listener)) {
            listeners.remove(listener);
        }
    }
}
