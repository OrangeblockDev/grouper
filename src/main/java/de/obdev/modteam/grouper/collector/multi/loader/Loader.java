package de.obdev.modteam.grouper.collector.multi.loader;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Projections;
import de.obdev.modteam.grouper.message.Message;
import de.obdev.modteam.grouper.watcher.Watcher;
import de.obdev.modteam.grouper.watcher.listener.TickListener;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.logging.Logger;

/**
 * Created by obdev on 11.07.2016.
 */
public class Loader implements Callable<List<Message>>, TickListener{

    Logger logger = Logger.getLogger(super.getClass().getName());

    MongoCollection<Document> collection;
    List<ObjectId> ids;

    Integer pos = 1;

    public Loader(MongoCollection<Document> collection, List<ObjectId> ids) {
        this.collection = collection;
        this.ids = ids;
        Watcher.addListener(this);
    }

    @Override
    public List<Message> call() throws Exception {

        logger.info("Started loader (" + System.identityHashCode(this) + ") with " + ids.size() + " ids");
        List<Message> messages = new ArrayList<>();



        for (ObjectId id: ids) {
            logger.fine("Loading message " + pos + " of " + ids.size());
            Document result = collection.find(
                    new Document("_id", id)
            ).projection(
                    Projections.include("_id", "message")
            ).first();

            messages.add(
                    new Message(
                            result.getObjectId("_id"),
                            result.getString("message")
                    )
            );

            result = null;

            pos++;
        }

        logger.info("Finished loader (" + System.identityHashCode(this) + ")");
        Watcher.removeListener(this);
        return messages;

    }

    @Override
    public void tick() {
        logger.info("Loading message " + pos + " of " + ids.size());
    }
}
