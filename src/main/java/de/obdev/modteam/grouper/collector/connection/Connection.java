package de.obdev.modteam.grouper.collector.connection;

import com.mongodb.client.MongoCollection;
import org.bson.Document;

/**
 * Created by obdev on 15.07.2016.
 */
public class Connection {

    MongoCollection<Document> storage;
    MongoCollection<Document> target;

    public Connection(MongoCollection storage, MongoCollection target) {
        this.storage = ((MongoCollection<Document>) storage);
        this.target = ((MongoCollection<Document>) target);
    }

    public MongoCollection<Document> getStorage() {
        return storage;
    }

    public MongoCollection<Document> getTarget() {
        return target;
    }
}
