package de.obdev.modteam.grouper.collector.multi;

import com.google.common.collect.Lists;
import com.mongodb.client.MongoCollection;
import de.obdev.modteam.grouper.collector.multi.loader.Loader;
import de.obdev.modteam.grouper.message.Message;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.logging.Logger;

/**
 * Created by obdev on 11.07.2016.
 */
public class MultiLoader implements Callable<List<Message>> {

    Logger logger = Logger.getLogger(super.getClass().getName());

    public Integer instances = Runtime.getRuntime().availableProcessors();

    public MongoCollection<Document> collection;



    public List<List<ObjectId>> lists;

    public ExecutorService service;

    public List<Future<List<Message>>> futures = new ArrayList<>();



    public MultiLoader(MongoCollection<Document> collection, List<ObjectId> ids) {


        this.collection = collection;

        lists = Lists.partition(ids, (ids.size()/instances));

        logger.info("Started MultiLoader with " + lists.size() + " lists and " + (ids.size()/instances) + " elements per list");

        service = Executors.newFixedThreadPool(instances);

    }

    @Override
    public List<Message> call() throws Exception {



        for (List<ObjectId> list:lists) {
            Loader loader = new Loader(
                    this.collection,
                    list
            );
            Future<List<Message>> future = this.service.submit(loader);

            futures.add(future);
        }

        List<Message> results = new ArrayList<>();

        for (Future<List<Message>> future: futures) {
            try {
                results.addAll(future.get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        service.shutdown();

        return results;

    }
}
