package de.obdev.modteam.grouper.collector;

import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Projections;
import de.obdev.modteam.grouper.collector.multi.MultiLoader;
import de.obdev.modteam.grouper.message.Message;
import de.obdev.modteam.grouper.watcher.Watcher;
import de.obdev.modteam.grouper.watcher.listener.TickListener;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by obdev on 08.07.2016.
 */
public class Collector implements TickListener{

    Logger logger = Logger.getLogger(super.getClass().getName());

    MongoCollection<Document> storage;
    MongoCollection<Document> target;
    Document storageOptions;
    Document targetOptions;

    Boolean loadAll = false;

    public String message;

    public MultiLoader multiLoader;

    public List<ObjectId> open;

    public Collector(MongoCollection<Document> storage, MongoCollection<Document> target, Document storageOptions, Document targetOptions) {
        this.storage = storage;
        this.target = target;
        this.storageOptions = storageOptions;
        this.targetOptions = targetOptions;
    }

    public Collector(MongoCollection<Document> storage, MongoCollection<Document> target, Document storageOptions, Document targetOptions, Boolean loadAll) {
        this.storage = storage;
        this.target = target;
        this.storageOptions = storageOptions;
        this.targetOptions = targetOptions;
        this.loadAll = loadAll;

        Watcher.addListener(this);
    }


    public List<Message> loadMessages() throws Exception {
        logger.info("Getting open messages");
        open = this.getOpen();
        logger.info("Loading content for open messages");
        multiLoader = new MultiLoader(this.storage, open);
        Watcher.removeListener(this);
        return multiLoader.call();

    }


    public List<ObjectId> getOpen() {
        List<ObjectId> inStorage = this.getIndex(storage, storageOptions);
        List<ObjectId> inTarget = this.getIndex(target, targetOptions);

        List<ObjectId> open = new ArrayList<>();
        if (loadAll) {
            open = inStorage;
        } else {

            for (ObjectId storageElement : inStorage) {
                if (!inTarget.contains(storageElement)) {
                    open.add(storageElement);
                    message = ("Comparing message " + open.size() + " of " + inStorage.size());
                }
            }

        }
        logger.info("Finally compared " + inStorage.size() + ", keeping " + open.size());

        return open;

    }

    public List<ObjectId> getIndex(MongoCollection<Document> collection, Document options) {

        Integer id = System.identityHashCode(collection);

        List<ObjectId> index = new ArrayList<>();

        FindIterable<Document> findIterable = collection.find(options).projection(
                Projections.include("_id")
        );
        findIterable.forEach(new Block<Document>() {
            @Override
            public void apply(final Document document) {
                index.add(document.getObjectId("_id"));
                message = ("Indexing message " + index.size() + " for collection " + id);
            }
        });

        logger.info("Finally loaded " + index.size() + " messages for collection " + System.identityHashCode(collection));

        return index;

    }

    @Override
    public void tick() {
        logger.info(message);
    }



}
