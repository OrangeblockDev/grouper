package de.obdev.modteam.grouper.generator.manager;

import com.google.common.base.Charsets;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import de.obdev.modteam.grouper.test.master.Master;
import info.debatty.java.stringsimilarity.KShingling;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Created by obdev on 10.07.2016.
 */
public class Manager {

    private static Logger logger = Logger.getLogger(Manager.class.getName());

    private static Gson gson = new Gson();

    private static File configFile = new File("generator.json");
    private static JsonObject config = new JsonObject();

    private static Boolean createdShingling = false;

    private static KShingling kShingling;

    static {



            logger.info("Loading configuration");
            try {
                config = gson.fromJson(
                        new FileReader(configFile),
                        JsonObject.class
                );
            } catch (Exception e) {
                config = new JsonObject();
            }

            /**
            try {
                kShingling = gson.fromJson(master.call("kShingling").getAsJsonObject(), KShingling.class);
            } catch (Exception e) {
                createdShingling = true;
                kShingling = Master.getkShingling();
                e.printStackTrace();
            }
             */

            kShingling = Master.getkShingling();


    }

    public static void saveConfig() throws IOException {
        logger.info("Updating configuration");
        config.add("kShingling", gson.toJsonTree(kShingling));
        FileUtils.write(configFile, gson.toJson(config), Charsets.UTF_8);

    }


    public static KShingling getkShingling() {
        return kShingling;
    }

    public static void setkShingling(KShingling kShingling) {
        Manager.kShingling = kShingling;
        try {
            saveConfig();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Boolean createdNewkShingling() {
        return createdShingling;
    }

    public static String getGeneratorId() {
        String ks = gson.toJson(kShingling);
        return DigestUtils.md5Hex(ks);
    }

}
