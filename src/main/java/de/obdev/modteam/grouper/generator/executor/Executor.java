package de.obdev.modteam.grouper.generator.executor;

import de.obdev.modteam.grouper.message.Message;
import info.debatty.java.stringsimilarity.KShingling;

import java.util.logging.Logger;

/**
 * Created by obdev on 07.07.2016.
 */
public class Executor implements Runnable{

    Logger logger = Logger.getLogger(super.getClass().getName());

    public KShingling kShingling;
    public Message message;
    public Boolean finished = false;

    public Executor(KShingling kShingling, Message message) {
        this.kShingling = kShingling;
        this.message = message;


    }

    //maxs97@mod-team.de

    @Override
    public void run() {

        try {


            message.setSet(
                    kShingling.getSet(
                            message.getMessage()
                    )
            );
        } catch (Exception e) {

        }
        finished = true;

        logger.fine("Finished progress for message " + message.getId());

    }

    public Message getMessage() {
        return message;
    }

    public Boolean isFinished() {
        return finished;
    }
}
