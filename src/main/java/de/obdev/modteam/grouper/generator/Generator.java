package de.obdev.modteam.grouper.generator;

import de.obdev.modteam.grouper.generator.executor.Executor;
import de.obdev.modteam.grouper.generator.multi.MultiCalculator;
import de.obdev.modteam.grouper.message.Message;
import de.obdev.modteam.grouper.watcher.Watcher;
import de.obdev.modteam.grouper.watcher.listener.TickListener;
import info.debatty.java.stringsimilarity.KShingling;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by obdev on 06.07.2016.
 */
public class Generator implements Runnable, TickListener {

    public Logger logger = Logger.getLogger(super.getClass().getName());

    public List<Message> messages = new ArrayList<>();
    public List<Executor> executors = new ArrayList<>();

    public List<Message> results = new ArrayList<>();

    public KShingling kShingling;

    public Integer pos = 0;

    public Integer instances = 48;

    public Boolean dead = false;


    public Generator(KShingling kShingling, List<Message> messages) {

        this.kShingling = kShingling;
        this.messages = messages;
        Watcher.addListener(this);
    }

    @Deprecated
    public List<Message> generate() throws Exception {
        MultiCalculator calculator = new MultiCalculator(
                kShingling,
                messages
        );

        return calculator.calculate();
    }


    @Override
    public void run() {
        while (!dead) {

            Integer amount = 0;
            Integer instances = this.instances;
            for (Executor executor : executors) {
                if (!executor.isFinished()) {
                    amount++;
                }
            }

            if (amount < instances) {

                while (instances > 0) {
                    if (pos < messages.size()) {
                        logger.fine("Creating new manager (instance " + instances + ") for message " + (pos + 1) + " of " + messages.size());
                        Executor executor = new Executor(kShingling, messages.get(pos));
                        Thread thread = new Thread(executor);
                        thread.setDaemon(true);
                        thread.start();
                        executors.add(executor);
                        pos++;
                        instances--;
                    } else {

                        while (!dead) {
                            Boolean finished = true;
                            for (Executor executor : executors) {
                                if (!executor.isFinished()) {
                                    finished = false;
                                }
                            }

                            if (finished) {
                                for (Executor executor : executors) {
                                    this.results.add(executor.getMessage());
                                }

                                this.dead = true;
                                logger.info("Finished progress for " + messages.size() + " messages");
                                Watcher.removeListener(this);
                            }
                        }
                    }

                    if (dead) {
                        break;
                    }
                }
            }
        }
    }


    public List<Message> getResults() {
        return results;
    }

    public KShingling getkShingling() {
        return kShingling;
    }

    @Override
    public void tick() {
        logger.info("Comparing message " + (pos + 1) + " of " + messages.size());
    }
}
