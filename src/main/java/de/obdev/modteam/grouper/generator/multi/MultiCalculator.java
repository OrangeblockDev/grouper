package de.obdev.modteam.grouper.generator.multi;

import com.google.common.collect.Lists;
import de.obdev.modteam.grouper.generator.multi.calculator.Calculator;
import de.obdev.modteam.grouper.message.Message;
import info.debatty.java.stringsimilarity.KShingling;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by obdev on 11.07.2016.
 */
public class MultiCalculator {

    Integer instances = Runtime.getRuntime().availableProcessors();

    List<List<Message>> lists;

    ExecutorService service;

    List<Future<List<Message>>> futures = new ArrayList<>();

    KShingling kShingling;

    public MultiCalculator(KShingling kShingling, List<Message> messages) {

        this.lists = Lists.partition(messages, (messages.size()/instances));

        this.service = Executors.newFixedThreadPool(instances);

        this.kShingling = kShingling;

    }



    public List<Message> calculate() throws Exception {

        for (List<Message> list:lists) {
            Calculator calculator = new Calculator(
                    kShingling,
                    list
            );
            Future<List<Message>> future = this.service.submit(calculator);

            System.out.println(lists.size());

            futures.add(future);
        }

        List<Message> results = new ArrayList<>();

        for (Future<List<Message>> future: futures) {
            try {
                results.addAll(future.get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        return results;
    }

    public KShingling getkShingling() {
        return this.kShingling;
    }


}
