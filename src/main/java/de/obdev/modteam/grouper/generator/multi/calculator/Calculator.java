package de.obdev.modteam.grouper.generator.multi.calculator;

import de.obdev.modteam.grouper.message.Message;
import info.debatty.java.stringsimilarity.KShingling;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by obdev on 11.07.2016.
 */
public class Calculator implements Callable<List<Message>>{

    KShingling kShingling;
    List<Message> messages;

    public Calculator(KShingling kShingling, List<Message> messages) {
        this.kShingling = kShingling;
        this.messages = messages;
    }

    @Override
    public List<Message> call() throws Exception {

        List<Message> results = new ArrayList<>();

        for (Message message:messages) {
            message.setSet(
                    kShingling.getSet(
                            message.getMessage()
                    )
            );
            results.add(message);
        }

        return results;

    }
}
