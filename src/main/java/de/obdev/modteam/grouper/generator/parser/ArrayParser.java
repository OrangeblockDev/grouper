package de.obdev.modteam.grouper.generator.parser;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Created by obdev on 10.07.2016.
 */
public class ArrayParser {

    Gson gson = new Gson();

    public ArrayParser() {
    }


    public JsonObject parse(JsonArray array) {
        JsonObject object = new JsonObject();

        Integer pos = 0;
        for (JsonElement element:array) {
            object.add(String.valueOf(pos), element);
            pos++;
        }
        return object;
    }

    public String parseToString(JsonArray array) {
        String out = gson.toJson(this.parse(array));
        return out;
    }
}
