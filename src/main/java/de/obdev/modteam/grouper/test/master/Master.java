package de.obdev.modteam.grouper.test.master;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mongodb.MongoClient;
import de.obdev.modteam.grouper.collector.connection.Connection;
import de.obdev.modteam.grouper.generator.manager.Manager;
import groovy.lang.GroovyShell;
import info.debatty.java.stringsimilarity.KShingling;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Created by obdev on 15.07.2016.
 */
public class Master {



    private static Logger logger = Logger.getLogger(Manager.class.getName());

    private static Gson gson = new Gson();

    private static File configFile = new File("grouper.json");
    private static JsonObject config = new JsonObject();

    private static GroovyShell shell = new GroovyShell();

    private static Connection connection;

    private static KShingling kShingling;

    static {

        try {
            logger.info("Loading configuration");
            config = gson.fromJson(
                    new FileReader(configFile),
                    JsonObject.class
            );

            connection = call("connection");
            kShingling = call("kShingling");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

    }

    public static Connection getConnection() {
        return connection;
    }


    public static KShingling getkShingling() {
        return kShingling;
    }






    public static <T> T call(String object) throws IOException {

        return (T) shell.evaluate(new File(config.get(object).getAsString()));

    }

    public static String getHash(String object) throws IOException {

        String code = FileUtils.readFileToString(new File(config.get(object).getAsString()), "UTF-8");
        return DigestUtils.md5Hex(code);

    }


}
