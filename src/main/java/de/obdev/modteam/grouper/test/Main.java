package de.obdev.modteam.grouper.test;

import com.google.common.base.Charsets;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import de.obdev.modteam.grouper.collector.Collector;
import de.obdev.modteam.grouper.collector.connection.Connection;
import de.obdev.modteam.grouper.generator.Generator;
import de.obdev.modteam.grouper.generator.manager.Manager;
import de.obdev.modteam.grouper.message.Message;
import de.obdev.modteam.grouper.serialization.Deserializer;
import de.obdev.modteam.grouper.serialization.Serializer;
import de.obdev.modteam.grouper.test.master.Master;
import de.obdev.modteam.grouper.transmittor.Transmitter;
import de.obdev.modteam.grouper.watcher.Watcher;
import de.obdev.modteam.similarity.kshingling.KShinglingLoader;
import de.obdev.modteam.similarity.kshingling.SparseBooleanVectorLoader;
import info.debatty.java.stringsimilarity.KShingling;
import info.debatty.java.stringsimilarity.StringSet;
import info.debatty.java.utils.SparseBooleanVector;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.io.*;
import java.lang.reflect.Field;
import java.util.*;
import java.util.logging.Logger;

/**
 * Created by obdev on 04.07.2016.
 */
public class Main {

    private static Logger logger = Logger.getLogger(Main.class.getName());

    private static Collector collector;

    private static Generator generator;
    private static Thread generatorThread;


    private static Transmitter transmitter;

    private static KShingling kShingling;

    public static void main(String[] args) throws Exception {

        Deserializer deserializer = new Deserializer();

        Gson gson = new Gson();
        String filein = FileUtils.readFileToString(new File("base.utf16.json"), Charsets.UTF_16);
        JsonObject in = gson.fromJson(filein, JsonObject.class).get("utf16").getAsJsonObject();

        System.out.println(in.size());

        Map<String, Integer> maps1 = new HashMap<>();
        Map<String, Integer> maps2 = new HashMap<>();

        for (Map.Entry<String, JsonElement> element:in.entrySet()) {
            String key = new String(Base64.decodeBase64(element.getKey()), Charsets.UTF_16);
            Integer value = element.getValue().getAsInt();

            maps1.put(key, value);

        }


        Connection connection = Master.getConnection();
        MongoCollection<Document> storage = connection.getStorage();
        MongoCollection<Document> target = connection.getTarget();

        KShinglingLoader dynamicKShingling = new KShinglingLoader(2, maps1);


        Document document1 = target.find(new Document("_id", new ObjectId("5724a48f6ae9226a80594678"))).first();
        List<Integer> integers1 = new ArrayList<>();

        for (Map.Entry<String, Object> entry1: document1.get("set", Document.class).entrySet()) {
            integers1.add(((Integer) entry1.getValue()));
        }





        Document document2 = target.find(new Document("_id", new ObjectId("5724a4b76ae9226a8059467c"))).first();
        List<Integer> integers2 = new ArrayList<>();

        for (Map.Entry<String, Object> entry2: document2.get("set", Document.class).entrySet()) {
            integers2.add(((Integer) entry2.getValue()));
        }

        SparseBooleanVector vector1 = new SparseBooleanVectorLoader(
                integers1
        ).getVector();

        SparseBooleanVector vector2 = new SparseBooleanVectorLoader(
                integers2
        ).getVector();

        StringSet set1 = new StringSet(vector1, dynamicKShingling.getkShingling());
        StringSet set2 = new StringSet(vector2, dynamicKShingling.getkShingling());

        System.out.println(
                set1.jaccardSimilarity(set2)
        );


        System.exit(1);

/**

        Watcher watcher = new Watcher();
        Thread tthread = new Thread(watcher);
        tthread.setDaemon(true);
        tthread.start();


        Connection connection = Master.getConnection();
        MongoCollection<Document> storage = connection.getStorage();
        MongoCollection<Document> target = connection.getTarget();

        collector = new Collector(
                storage,
                target,
                new Document(),
               //new Document("generatorId", new Document("$ne", Manager.getGeneratorId())),
                new Document(),
                true
        );


        generator = new Generator(Manager.getkShingling(), collector.loadMessages());
        generatorThread = new Thread(generator);

        generatorThread.start();

        logger.info("Now waiting for finishing generation progress");

        generatorThread.join();


        kShingling = generator.getkShingling();
        Manager.setkShingling(kShingling);

        Field field = kShingling.getClass().getDeclaredField("shingles");
        field.setAccessible(true);

        Map<String, Integer> map = (Map<String, Integer>) field.get(kShingling);

        Serializer serializer = new Serializer();

        String data = serializer.toString(map);
        FileUtils.write(new File("output/byte.utf8.json"), data, Charsets.UTF_8);




        JsonObject outUTF8 = new JsonObject();
        JsonObject outUTF16 = new JsonObject();
        JsonObject outISO = new JsonObject();

        JsonObject safeOutUTF8 = new JsonObject();
        JsonObject safeOutUTF16 = new JsonObject();
        JsonObject safeOutISO = new JsonObject();

        for (String key:map.keySet()) {
            Integer value = map.get(key);

            String utf8Key = Base64.encodeBase64String(key.getBytes(Charsets.UTF_8));
            String utf16Key = Base64.encodeBase64String(key.getBytes(Charsets.UTF_16));
            String isoKey = Base64.encodeBase64String(key.getBytes(Charsets.ISO_8859_1));

            String utf8Safe = Base64.encodeBase64URLSafeString(key.getBytes(Charsets.UTF_8));
            String utf16Safe = Base64.encodeBase64URLSafeString(key.getBytes(Charsets.UTF_16));
            String isoSafe = Base64.encodeBase64URLSafeString(key.getBytes(Charsets.ISO_8859_1));


            outUTF8.addProperty(utf8Key, value);
            outUTF16.addProperty(utf16Key, value);
            outISO.addProperty(isoKey, value);

            safeOutUTF8.addProperty(utf8Safe, value);
            safeOutUTF16.addProperty(utf16Safe, value);
            safeOutISO.addProperty(isoSafe, value);
        }

        JsonObject finalOut = new JsonObject();
        finalOut.add("utf8", outUTF8);
        finalOut.add("utf16", outUTF16);
        finalOut.add("iso_8859_1", outISO);

        finalOut.add("safeutf8", safeOutUTF8);
        finalOut.add("safeutf16", safeOutUTF16);
        finalOut.add("safeiso_8859_1", safeOutISO);

        FileUtils.write(new File("output/base.utf8.json"), gson.toJson(finalOut), Charsets.UTF_8);
        FileUtils.write(new File("output/base.utf16.json"), gson.toJson(finalOut), Charsets.UTF_16);
        FileUtils.write(new File("output/base.iso_8859_1.json"), gson.toJson(finalOut), Charsets.ISO_8859_1);

        transmitter = new Transmitter(target, generator.getResults(), Manager.getGeneratorId());
        transmitter.save();

 **/

    }
    public static void none() throws InterruptedException {


        MongoClient client = new MongoClient("149.202.182.156");
        MongoCollection<Document> collection = client.getDatabase("storage").getCollection("messages");

        Integer toFetch = 100;

        Integer skip = 200;


        List<Document> docs = new ArrayList<>();

        Integer pos = 0;
        while (docs.size() < toFetch) {
            pos = pos + skip;
            docs.add(collection.find().limit(-1).skip(pos).first());
            System.out.println(docs.size());
        }


        List<Message> messages = new ArrayList<>();

        for (Document document:docs) {

            Message message = new Message(document.getObjectId("_id"), document.getString("message"));
            messages.add(message);

        }

        Generator generator = new Generator(Manager.getkShingling(), messages);
        Thread thread = new Thread(generator);

        thread.start();


        thread.join();


        Manager.setkShingling(generator.getkShingling());

    }


}
