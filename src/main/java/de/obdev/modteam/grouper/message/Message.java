package de.obdev.modteam.grouper.message;

import info.debatty.java.stringsimilarity.StringSet;
import org.bson.types.ObjectId;

/**
 * Created by obdev on 07.07.2016.
 */
public class Message {

    private ObjectId id;
    private String message;
    private StringSet set;

    public Message(ObjectId id, String message) {
        this.id = id;
        this.message = message;
        this.set = set;
    }

    public void setSet(StringSet set) {
        this.set = set;
    }

    public StringSet getSet() {
        return this.set;
    }

    public ObjectId getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }
}
