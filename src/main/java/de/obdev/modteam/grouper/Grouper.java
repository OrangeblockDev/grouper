package de.obdev.modteam.grouper;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import org.bson.Document;

import java.io.UnsupportedEncodingException;

/**
 * Created by obdev on 04.07.2016.
 */



public class Grouper  {

    Gson gson = new Gson();

    public Grouper() throws UnsupportedEncodingException {

        JsonArray array = new JsonArray();
        array.add(1);
        array.add(2);
        array.add(3);

        Document document = Document.parse(gson.toJson(array, JsonArray.class));
        System.out.println(document);

    }

}
