package de.obdev.modteam.grouper.serialization;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.io.*;

/**
 * Created by obdev on 18.07.2016.
 */
public class Serializer {

    private Gson gson = new Gson();

    public byte[] toArray(Object object) throws IOException {

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out = null;
        try {
            out = new ObjectOutputStream(bos);
            out.writeObject(object);
            byte[] bytes = bos.toByteArray();
            return bytes;
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException ex) {
                // ignore close exception
            }
            try {
                bos.close();
            } catch (IOException ex) {
                // ignore close exception
            }
        }

    }



    public String toString(Object object) throws IOException {
        return gson.toJson(this.toArray(object));
    }



}
