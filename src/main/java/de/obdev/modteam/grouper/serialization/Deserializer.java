package de.obdev.modteam.grouper.serialization;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.util.Arrays;

/**
 * Created by obdev on 18.07.2016.
 */
public class Deserializer {

    public Gson gson = new Gson();

    public <T> T fromArray(byte[] array) throws IOException, ClassNotFoundException {
        ByteArrayInputStream bis = new ByteArrayInputStream(array);
        ObjectInput in = null;
        try {
            in = new ObjectInputStream(bis);
            Object object = in.readObject();
            return ((T) object);
        } finally {
            try {
                bis.close();
            } catch (IOException ex) {
                // ignore close exception
            }
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                // ignore close exception
            }
        }
    }



    public <T> T fromString(String string) throws IOException, ClassNotFoundException {
        System.out.println(string);
        byte[] array = gson.fromJson(string, byte[].class);


        return this.fromArray(array);
    }

}
