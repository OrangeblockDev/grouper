package de.obdev.modteam.grouper.transmittor.multi;

import com.google.common.collect.Lists;
import com.mongodb.client.MongoCollection;
import de.obdev.modteam.grouper.message.Message;
import de.obdev.modteam.grouper.transmittor.multi.saver.Saver;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.logging.Logger;

/**
 * Created by obdev on 11.07.2016.
 */
public class MultiSaver implements Callable {

    Logger logger = Logger.getLogger(super.getClass().getName());

    public Integer instances = Runtime.getRuntime().availableProcessors();

    public MongoCollection<Document> collection;

    public String generatorId;

    public List<List<Message>> lists;

    public ExecutorService service;

    public List<Future> futures = new ArrayList<>();


    public MultiSaver(MongoCollection<Document> collection, List<Message> messages, String generatorId) {

        this.collection = collection;

        this.lists = Lists.partition(messages, (messages.size()/instances));

        this.generatorId = generatorId;

        logger.info("Started MultiLoader with " + lists.size() + " lists and " + (messages.size()/instances) + " elements per list");

        service = Executors.newFixedThreadPool(instances);

    }


    @Override
    public Object call() throws Exception {

        for (List<Message> list:lists) {

            Saver saver = new Saver(
                    this.collection,
                    list,
                    generatorId
            );

            Future<Object> future = this.service.submit(saver);

            futures.add(future);

        }

        for (Future<List<Message>> future: futures) {
            try {
                future.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        service.shutdown();

        return null;

    }
}
