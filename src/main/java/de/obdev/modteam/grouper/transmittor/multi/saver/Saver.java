package de.obdev.modteam.grouper.transmittor.multi.saver;

import com.google.gson.Gson;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.result.UpdateResult;
import de.obdev.modteam.grouper.generator.parser.ArrayParser;
import de.obdev.modteam.grouper.message.Message;
import de.obdev.modteam.grouper.watcher.Watcher;
import de.obdev.modteam.grouper.watcher.listener.TickListener;
import org.bson.Document;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.logging.Logger;

/**
 * Created by obdev on 11.07.2016.
 */
public class Saver implements Callable, TickListener{

    Logger logger = Logger.getLogger(super.getClass().getName());

    MongoCollection<Document> collection;
    List<Message> messages;
    String generatorId;

    Gson gson = new Gson();
    ArrayParser parser = new ArrayParser();

    Integer pos = 1;

    public Saver(MongoCollection<Document> collection, List<Message> messages, String generatorId) {
        this.collection = collection;
        this.messages = messages;
        this.generatorId = generatorId;

        Watcher.addListener(this);

    }

    @Override
    public Object call() throws Exception {

        logger.info("Started saver (" + System.identityHashCode(this) + ") with " + messages.size() + " ids");


        for (Message message:messages) {



            String jsonArray = parser.parseToString(
                    gson.toJsonTree(
                            message.getSet()
                    )
                            .getAsJsonObject()
                            .get("vector")
                            .getAsJsonObject()
                            .get("keys")
                            .getAsJsonArray()
            );

            Document document = new Document(
                    "set",
                    Document.parse(jsonArray)
            ).append(

                    "message",
                    message.getMessage()

            ).append(

                    "generatorId",
                    generatorId

            );


            UpdateResult result = collection.replaceOne(
                    new Document(
                            "_id",
                            message.getId()
                    ),
                    document,
                    (new UpdateOptions()).upsert(true)
            );

            pos++;


        }

        logger.info("Finished saver (" + System.identityHashCode(this) + ")");
        Watcher.removeListener(this);
        return null;

    }


    @Override
    public void tick() {
        logger.info("Storing message " + pos + " of " + messages.size());
    }
}
