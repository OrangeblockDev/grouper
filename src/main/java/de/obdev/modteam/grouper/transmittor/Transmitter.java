package de.obdev.modteam.grouper.transmittor;

import com.google.gson.Gson;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.result.UpdateResult;
import de.obdev.modteam.grouper.generator.parser.ArrayParser;
import de.obdev.modteam.grouper.message.Message;
import de.obdev.modteam.grouper.transmittor.multi.MultiSaver;
import org.bson.Document;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by obdev on 10.07.2016.
 */
public class Transmitter {

    Logger logger = Logger.getLogger(super.getClass().getName());


    Gson gson = new Gson();

    public MongoCollection<Document> target;
    public List<Message> messages = new ArrayList<>();
    public String generatorId;

    public MultiSaver multiSaver;

    public Transmitter(MongoCollection<Document> target, List<Message> messages, String generatorId) {
        this.target = target;
        this.messages = messages;
        this.generatorId = generatorId;

    }

    public void store() throws UnsupportedEncodingException {

        ArrayParser parser = new ArrayParser();


        for (Message message : messages) {

            String jsonArray = parser.parseToString(
                    gson.toJsonTree(
                            message.getSet()
                    )
                            .getAsJsonObject()
                            .get("vector")
                            .getAsJsonObject()
                            .get("keys")
                            .getAsJsonArray()
            );

            Document document = new Document(
                    "set",
                    Document.parse(jsonArray)
            ).append(

                    "message",
                    message.getMessage()

            ).append(

                    "generatorId",
                    generatorId

            );


            UpdateResult result = target.replaceOne(
                    new Document(
                            "_id",
                            message.getId()
                    ),
                    document,
                    (new UpdateOptions()).upsert(true)
            );

            logger.info("Storing message " + (messages.indexOf(message)+1) + " of " + messages.size());


        }


    }

    public void save() throws Exception {
        multiSaver = new MultiSaver(this.target, this.messages, this.generatorId);
        multiSaver.call();
    }
}
