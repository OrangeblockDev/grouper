package de.obdev.modteam.similarity.kshingling;

import com.google.common.primitives.Ints;
import info.debatty.java.utils.SparseBooleanVector;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by obdev on 20.07.2016.
 */
public class SparseBooleanVectorLoader {

    SparseBooleanVector vector;

    public SparseBooleanVectorLoader(int[] array) throws IllegalAccessException, NoSuchFieldException {
        vector = new SparseBooleanVector();
        Field field = vector.getClass().getDeclaredField("keys");
        field.setAccessible(true);
        field.set(vector, array);
    }

    public SparseBooleanVectorLoader(List<Integer> keys) throws IllegalAccessException, NoSuchFieldException {
        vector = new SparseBooleanVector();
        Field field = vector.getClass().getDeclaredField("keys");
        field.setAccessible(true);
        field.set(vector, Ints.toArray(keys));
    }

    public SparseBooleanVector getVector() {
        return vector;
    }
}
