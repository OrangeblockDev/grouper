package de.obdev.modteam.similarity.kshingling;

import info.debatty.java.stringsimilarity.KShingling;

import java.lang.reflect.Field;
import java.util.Map;

/**
 * Created by obdev on 20.07.2016.
 */
public class KShinglingLoader extends KShingling{

    KShingling kShingling;

    public KShinglingLoader(Integer integer, Map<String, Integer> map) throws NoSuchFieldException, IllegalAccessException {
        kShingling = new KShingling(integer);

        Field field = kShingling.getClass().getDeclaredField("shingles");
        field.setAccessible(true);
        field.set(kShingling, map);

    }

    public KShingling getkShingling() {
        return kShingling;
    }
}
